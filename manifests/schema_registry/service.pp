class confluent::schema_registry::service (
  $daemonpath = '/usr/bin',
  $configpath = '/etc/schema-registry',
  $daemonname = 'schema-registry',
  $propertyname = 'schema-registry.properties',
  $pidpattern = '[s]chema-registry',
 ) {

  file { '/etc/init.d/kafka-schema_registry':
    ensure  => file,
    mode    => '0755',
    content => template('confluent/init.erb'),
  }

  file { '/var/log/schema-registry':
    ensure => 'directory',
  }

   service { 'kafka-schema_registry':
    ensure  => running,
    enable  => true,
    require => [File['/var/log/schema-registry'], File['/etc/init.d/kafka-schema_registry'], Service["kafka-rest"]],
    provider => 'init',
  }

}