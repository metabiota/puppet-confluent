class confluent::kafka_rest::service (
  $daemonpath = '/usr/bin',
  $configpath = '/etc/kafka-rest',
  $daemonname = 'kafka-rest',
  $propertyname = 'kafka-rest.properties',
  $pidpattern = '[k]afka-rest',
){

  file { '/etc/init.d/kafka-rest':
    ensure  => file,
    mode    => '0755',
    content => template('confluent/init.erb'),
  }

  file { '/var/log/kafka-rest':
    ensure => 'directory',
  }

  exec {"sleep-60": 
    command => "sleep 60",
    path=>"/usr/bin:/usr/sbin:/bin:/sbin", 
    require=>Service["kafka-server"],
    }

  service { 'kafka-rest':
    ensure  => running,
    enable  => true,
    require => [File['/var/log/kafka-rest'], File['/etc/init.d/kafka-rest'], Exec['sleep-60']],
    provider => 'init',
  }

}